<?php

namespace App\Http\Controllers;

use App\Http\Requests\ReRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class RequestController extends Controller
{
    public function message(ReRequest $request)
    {
        $order = \App\Models\Request::create($request->all());
//        Mail::send('mail.mail', ['request' => $order], function ($message) {
//            $message->to('consulting_group@gmail.com', 'Business Consulting')->subject('Заявка');
//            $message->from('info@consulting.com','itpark.network');
//        });
        return response(['data'=>$order],200);
    }
}
